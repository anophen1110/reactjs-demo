import _ from 'lodash';

const Utils = {
  escapeString: function(string) {
    return string.replace(/[^A-Z0-9\.]+/ig, "_");
  },
  buildUrlWithParams: function(url, params, removeEncode){
    let ret = '';
    url += '?';
    for (var d in params) {
      if (params[d] || params[d] === 0) {
        if(removeEncode){
          ret += d + '=' + params[d] + '&';
        }else{
          if(_.isArray(params[d])) {
            _.forEach(params[d], (item) => { ret += encodeURIComponent(d) + '=' + encodeURIComponent(item) + '&'});
          }
          else {
            ret += encodeURIComponent(d) + '=' + encodeURIComponent(params[d]) + '&';
          }
        }
      }
    }
    ret = ret.replace(/&$/, '');
    return url + ret;
  },
  stringInject: function(str, arr) {
    if (typeof str !== 'string' || !(arr instanceof Array)) {
      return false;
    }

    return str.replace(/({\d})/g, function(i) {
      return arr[i.replace(/{/, '').replace(/}/, '')];
    })
  },
};

export default Utils;
