import axios from 'axios';
import { AppConfig, Env, Endpoints } from '../constants';
import _ from 'lodash';
import Utils from '../utils';
import {connect} from 'react-redux';

const getInstance = () => {
  const instance = axios.create({
    baseURL: AppConfig.API_BASE_URL,
    timeout: 30000,
  });
  return instance;
};
const API = {
  instance: getInstance(),
};


API.getProduct = (params) =>{
  return API.instance
    .get(Endpoints.PRODUCT, params)
    .then(response => response.data)
    .catch(error =>{
      throw error;
    });
}

API.deleteProduct = (params) =>{
  return API.instance
    .delete(`${Endpoints.PRODUCT}/${params}`)
    .then(response => response.data)
    .catch(error =>{
      throw error;
    });
}

export default API;




