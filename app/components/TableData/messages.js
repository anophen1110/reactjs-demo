/*
 * TableData Messages
 *
 * This contains all the text for the TableData component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.TableData';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the TableData component!',
  },
});
