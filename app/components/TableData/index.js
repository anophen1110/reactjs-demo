/**
 *
 * TableData
 *
 */

import React, { memo } from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import DataTable from 'react-data-table-component';
import {Table} from 'reactstrap';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

function TableData(props) {
  const noDataComponent = (
    <div>
      <Table>
        <thead>
          <tr>
            {props && props.columns &&props.columns.map((item, index)=>(
              <th key={index}>{item.name}</th>
              ))}
          </tr>
        </thead>
      </Table>
      <div>Không có dữ liệu</div>
    </div>
  );
  return (
    <div>
      <DataTable {...props} noDataComponent={noDataComponent}/>
    </div>
  );
}

TableData.propTypes = {};

export default memo(TableData);
