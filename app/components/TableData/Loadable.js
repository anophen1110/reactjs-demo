/**
 *
 * Asynchronously loads the component for TableData
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
