import styled from 'styled-components';
import {Button} from 'reactstrap';

export const StyledButton = styled(Button)`
  padding: 5px 15px;
  font-size: 13px;
  border-radius: 5px;
  border:none;
  outline:none;
  margin-top:5px;
`;
