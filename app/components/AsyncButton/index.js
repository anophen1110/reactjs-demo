/**
 *
 * AsyncButton
 *
 */

import React, { memo } from 'react';
import {StyledButton}  from './styledButton';
import {Button} from 'reactstrap';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

function AsyncButton(props) {
  return (
    <div>
      <StyledButton {...props}/>
    </div>
  );
}

AsyncButton.propTypes = {};

export default memo(AsyncButton);
