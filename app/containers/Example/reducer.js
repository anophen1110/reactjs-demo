/*
 *
 * Example reducer
 *
 */
import produce from 'immer';
import { DEFAULT_ACTION, GET_PRODUCT_SUCCESS, GET_PRODUCT, DELETE_PRODUCT_SUCCESS, DELETE_PRODUCT } from './constants';

export const initialState = {
  loadingProduct:false,
  productList:[],
  clearSelectedRows: false,
  reloadTable: true
};

/* eslint-disable default-case, no-param-reassign */
const exampleReducer = (state = initialState, action) =>
  produce(state, ( draft) => {
    switch (action.type) {
      case DEFAULT_ACTION:
        break;
      case GET_PRODUCT:
        draft.loadingProduct = true;
        break;
      case GET_PRODUCT_SUCCESS:
        draft.loadingProduct = false;
        draft.productList = action.response;
        break;
      case DELETE_PRODUCT_SUCCESS:
        draft.loadingProduct = false;
        draft.clearSelectedRows = !draft.clearSelectedRows;
        draft.reloadTable = !draft.reloadTable;
        break;
    }
  });

export default exampleReducer;
