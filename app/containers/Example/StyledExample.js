import styled from 'styled-components';

export const ExampleStyled = styled.div`
  background-color:#fff;
  box-shadow: 3px 3px 10px -2px #333;
  padding:10px;
  font-family: 'Regular';
  input{
    background-color:#f5f5f5;
    margin-bottom:5px;  
    padding:5px; 
  }
`;
