import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the example state domain
 */

const selectExampleDomain = state => state.example || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by Example
 */

const makeSelectExample = () =>
  createSelector(
    selectExampleDomain,
    substate => substate,
  );
const makeSelectGetProductList = () =>
  createSelector(
    selectExampleDomain,
    substate => substate.productList,
  );

const makeSelectDeleteProduct = () =>
  createSelector(
    selectExampleDomain,
    substate => substate.clearSelectedRows,
  );
const makeSelectReloadTableData = () =>
  createSelector(
    selectExampleDomain,
    substate => substate.reloadTable,
  );

export default makeSelectExample;
export { selectExampleDomain, makeSelectGetProductList, makeSelectDeleteProduct, makeSelectReloadTableData };
