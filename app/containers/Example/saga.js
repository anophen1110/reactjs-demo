import { takeLatest, call, put, select } from 'redux-saga/effects';
import { push } from 'connected-react-router';
import {API} from "../../network";
import {getProductSuccess, deleteProductSuccess} from './actions';
import {GET_PRODUCT, DELETE_PRODUCT} from './constants';
// Individual exports for testing

export function* getProductList(action) {
  try{
    const resp = yield call(API.getProduct, action.params);
    yield put(getProductSuccess(resp));
  }catch (e) {

  }
}

export function* deleteProduct(action) {
  try{
    const resp = yield call(API.deleteProduct, action.params);
    if(resp){
      yield put(deleteProductSuccess(resp));
    }else{
    }

  }catch (e) {

  }
}

export default function* exampleSaga() {
  yield takeLatest(GET_PRODUCT, getProductList);
  yield takeLatest(DELETE_PRODUCT, deleteProduct);
}
