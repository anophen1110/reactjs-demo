/**
 *
 * Example
 *
 */

import React, { memo, useState, useEffect } from 'react';
import useForm from 'react-hook-form';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { format, fromUnixTime } from 'date-fns';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import {ExampleStyled} from './StyledExample';
import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectExample, {makeSelectGetProductList, makeSelectDeleteProduct, makeSelectReloadTableData} from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import {getProduct, deleteProduct} from './actions'
import _ from 'lodash';
import axios from 'axios';
import TableData from 'components/TableData';
import AsyncButton from 'components/AsyncButton';
import {Button} from 'reactstrap';

export function Example({
  loadProductList,
  productList,
  example,
  clearSelectedRows,
  deleteProduct,
  reloadTable,
  match
                        }) {
  const methods= useForm();
  useInjectReducer({ key: 'example', reducer });
  useInjectSaga({ key: 'example', saga });
  const [number, setNumber] = useState(0);
  const [arr, setArr] = useState([]);
  const [sum, setSum] = useState(0);
  const [selectedRows, setSelectedRows] = useState([]);
  const [selectedId, setSelectedId] = useState(null);

  const productColumns = [
    {
      name:'#',
      selector:'id',
      center:true,
      width: '10rem'
    },
    {
      name:'name',
      selector:'name',
      center:true,
      cell: row => <div><Link to={`/example/${row.id}`}>{row.name}</Link></div>
    },
    {
      name:'price',
      selector:'price',
      right:true
    },
    {
      name:'createDate',
      selector:'createDate',
      center:true,
      cell: row => row.createDate? format(fromUnixTime(row.createDate/1000),'dd/mm/yyyy'):""
    },
  ];
  useEffect(()=>{
    console.log(match.params.id)
  },[match.params.id]);
  const addOne = (e) =>{
    e.preventDefault();
    setNumber(number+1);
  };
  const onSubmit = (data) =>{
    let list = _.map(data,(value,index)=>{
      if(value==""){
        value=0;
      }
      return value;
    });
    let total = _.reduce(list,  (memoizer, number)=> {
      return parseInt(memoizer) + parseInt(number);
    });
    setSum(total);
  };

  useEffect(()=>{
    loadProductList([]);
  },[]);

  const handleChange = (state) =>{
    let id = _.map(state.selectedRows,(item,index)=>{
      setSelectedId(item.id);
    })
  };
  const handleClickOk = () =>{
    deleteProduct(selectedId);
  };

  useEffect(() => {
    setSelectedRows([]);
    if (!clearSelectedRows) return;
    loadProductList();
  }, [reloadTable]);

  return (
    <ExampleStyled>
      <form onSubmit={methods.handleSubmit(onSubmit)}>
        <button onClick={addOne}>Cộng thêm 1</button>
        <p>Number:{number}//////////{sum}</p>
        Số thứ 1:<input type="number" name="no1" placeholder="input number" ref={methods.register}/><br/>
        Số thứ 2:<input type="number" name="no2" placeholder="input number" ref={methods.register}/><br/>
        Số thứ 3:<input type="number" name="no3" placeholder="input number" ref={methods.register}/><br/>
        Số thứ 4:<input type="number" name="no4" placeholder="input number" ref={methods.register}/><br/>
        <button type="submit">OK</button>
      </form>
      <AsyncButton color="danger" onClick={handleClickOk}>Delete</AsyncButton>
      <Link to={`${match.path}/new`}><AsyncButton color="success" onClick={handleClickOk}>Add</AsyncButton></Link>

      <TableData
        title=""
        noHeader={true}
        columns={productColumns}
        data={productList}
        pagination
        selectableRows
        onRowSelected={handleChange}
        clearSelectedRows={clearSelectedRows}
        progressPending={example.loadingProduct}
      />
    </ExampleStyled>
  );
}

Example.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  example: makeSelectExample(),
  productList: makeSelectGetProductList(),
  clearSelectedRows: makeSelectDeleteProduct(),
  reloadTable: makeSelectReloadTableData()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    loadProductList: params =>{
      dispatch(getProduct(params));
    },
    deleteProduct: params =>{
      dispatch(deleteProduct(params));
    }
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(Example);
