/*
 *
 * Example actions
 *
 */

import { DEFAULT_ACTION, GET_PRODUCT, GET_PRODUCT_SUCCESS, DELETE_PRODUCT, DELETE_PRODUCT_SUCCESS } from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function getProduct(params) {
  return {
    type: GET_PRODUCT,
    params
  };
}

export function getProductSuccess(response) {
  return {
    type: GET_PRODUCT_SUCCESS,
    response
  };
}

export function deleteProduct(params) {
  return {
    type: DELETE_PRODUCT,
    params
  };
}

export function deleteProductSuccess(response) {
  return {
    type: DELETE_PRODUCT_SUCCESS,
    response
  };
}
