/*
 *
 * Example constants
 *
 */

export const DEFAULT_ACTION = 'app/Example/DEFAULT_ACTION';

export const GET_PRODUCT = 'app/Example/GET_PRODUCT';
export const GET_PRODUCT_SUCCESS = 'app/Example/GET_PRODUCT_SUCCESS';

export const DELETE_PRODUCT = 'app/Example/DELETE_PRODUCT';
export const DELETE_PRODUCT_SUCCESS = 'app/Example/DELETE_PRODUCT_SUCCESS';
