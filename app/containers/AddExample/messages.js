/*
 * AddExample Messages
 *
 * This contains all the text for the AddExample container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.AddExample';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the AddExample container!',
  },
});
