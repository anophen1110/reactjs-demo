/**
 *
 * AddExample
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectAddExample from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

export function AddExample() {
  useInjectReducer({ key: 'addExample', reducer });
  useInjectSaga({ key: 'addExample', saga });

  return (
    <div>
      <Helmet>
        <title>AddExample</title>
        <meta name="description" content="Description of AddExample" />
      </Helmet>
      <FormattedMessage {...messages.header} />
    </div>
  );
}

AddExample.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  addExample: makeSelectAddExample(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(AddExample);
