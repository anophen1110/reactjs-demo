/**
 *
 * Asynchronously loads the component for AddExample
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
