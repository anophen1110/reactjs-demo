import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the addExample state domain
 */

const selectAddExampleDomain = state => state.addExample || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by AddExample
 */

const makeSelectAddExample = () =>
  createSelector(
    selectAddExampleDomain,
    substate => substate,
  );

export default makeSelectAddExample;
export { selectAddExampleDomain };
