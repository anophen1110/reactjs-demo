/**
 *
 * Stepper
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectStepper from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

export function Stepper() {
  useInjectReducer({ key: 'stepper', reducer });
  useInjectSaga({ key: 'stepper', saga });

  return (
    <div>
      <Helmet>
        <title>Stepper</title>
        <meta name="description" content="Description of Stepper" />
      </Helmet>
      <FormattedMessage {...messages.header} />
    </div>
  );
}

Stepper.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  stepper: makeSelectStepper(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(Stepper);
