import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the stepper state domain
 */

const selectStepperDomain = state => state.stepper || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by Stepper
 */

const makeSelectStepper = () =>
  createSelector(
    selectStepperDomain,
    substate => substate,
  );

export default makeSelectStepper;
export { selectStepperDomain };
