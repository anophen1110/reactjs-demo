/*
 * Stepper Messages
 *
 * This contains all the text for the Stepper container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.Stepper';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the Stepper container!',
  },
});
